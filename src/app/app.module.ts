import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { BottlesComponent } from './bottles/bottles.component';
import { BottleDetailComponent } from './bottles/bottle-detail/bottle-detail.component';
import { BottleEditComponent } from './bottles/bottle-edit/bottle-edit.component';
import { BottleListComponent } from './bottles/bottle-list/bottle-list.component';
import { BottleItemComponent } from './bottles/bottle-list/bottle-item/bottle-item.component';
import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from './app-routing.module';
import { BottleService } from './bottle/bottle.service';
import { BottleListService } from './bottles/bottle-list/bottle-list.service';

@NgModule({
  declarations: [
    AppComponent,
    BottlesComponent,
    BottleDetailComponent,
    BottleEditComponent,
    BottleListComponent,
    BottleItemComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [BottleService, BottleListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
