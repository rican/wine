import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Bottle } from '../../bottle/bottle.model';
import { BottleService } from '../../bottle/bottle.service';
import { BottleListService } from '../bottle-list/bottle-list.service';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-bottle-edit',
  templateUrl: './bottle-edit.component.html',
  styleUrls: ['./bottle-edit.component.css']
})
export class BottleEditComponent implements OnInit {
  subscription: Subscription;
  editMode = false;
  editedBottleIndex: number;
  editedBottle: Bottle;

  @ViewChild('f') bottleForm: NgForm;

  constructor(private bottleService: BottleService,
              private blService: BottleListService,
              private route: ActivatedRoute,
              private router: Router) {}

  ngOnInit() {
    this.subscription = this.blService.startedEditing
      .subscribe(
          (index: number) => {
            this.editedBottleIndex = index;
            this.editMode = true;
            this.editedBottle = this.blService.getBottle(index);
            this.bottleForm.setValue({
                            name: this.editedBottle.name,
                            year: this.editedBottle.year,
                            country: this.editedBottle.country,
                            region:this.editedBottle.region,
                            aoc:this.editedBottle.aoc,
                            num:this.editedBottle.number
                          })
          }
      );
  }

  onClear() {
    this.bottleForm.reset();
    this.editMode = false;
  }

  onDelete(index: number) {
    this.blService.deleteBottle(this.editedBottleIndex);
    this.router.navigate(['/bottles']);
  }

  onSubmit(form: NgForm) {
    const value = form.value;
    const newBottle = new Bottle(value.name, value.year, value.country, value.region, value.aoc, value.num);
    if (this.editMode) {
      this.blService.updateBottle(this.editedBottleIndex, newBottle);
      this.router.navigate(['/bottles']);
    } else {
      this.blService.addBottle(newBottle);
    };
    this.onClear();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
