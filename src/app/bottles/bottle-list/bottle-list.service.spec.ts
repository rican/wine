import { TestBed, inject } from '@angular/core/testing';

import { BottleListService } from './bottle-list.service';

describe('BottleListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BottleListService]
    });
  });

  it('should ...', inject([BottleListService], (service: BottleListService) => {
    expect(service).toBeTruthy();
  }));
});
