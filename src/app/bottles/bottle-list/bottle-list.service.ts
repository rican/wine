import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Bottle } from '../../bottle/bottle.model';


@Injectable()
export class BottleListService {

bottleChanged = new Subject<Bottle[]>();
startedEditing = new Subject<number>();

private bottles: Bottle[] = [
  new Bottle(
    'Chateau Beychevelle',
    1999,
    'France',
    'Bordeaux',
    'Saint Julien',
    12
  ),

  new Bottle(
    'Chateau Romanée Conti',
    2008,
    'France',
    'Bourgogne',
    'Côtes de Nuit',
    3
  ),
];

  constructor() { }

  getBottle(index: number) {
    return this.bottles[index];
  }

  getBottles() {
    return this.bottles.slice();
  }

  addBottle(bottle: Bottle) {
    this.bottles.push(bottle);
    this.bottleChanged.next(this.bottles.slice());
  }

  updateBottle(index: number, newBottle: Bottle) {
    this.bottles[index] = newBottle;
    this.bottleChanged.next(this.bottles.slice());
  }

  deleteBottle(index:number) {
    this.bottles.splice(index, 1);
    this.bottleChanged.next(this.bottles.slice());
  }

}
