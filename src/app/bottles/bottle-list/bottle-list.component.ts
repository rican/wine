import { Component, OnInit } from '@angular/core';
import { Bottle } from '../../bottle/bottle.model';
import { ActivatedRoute, Router } from '@angular/router';
import { BottleService } from '../../bottle/bottle.service';
import { Subscription } from 'rxjs/Subscription';
import { BottleListService } from './bottle-list.service';


@Component({
  selector: 'app-bottle-list',
  templateUrl: './bottle-list.component.html',
  styleUrls: ['./bottle-list.component.css']
})
export class BottleListComponent implements OnInit {

  bottles: Bottle[];
  subscription: Subscription;


  constructor(private bottleService: BottleService,
              private blService: BottleListService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.subscription = this.blService.bottleChanged
      .subscribe(
        (bottles: Bottle[]) => {
          this.bottles = bottles;
        }
      );
    this.bottles = this.blService.getBottles();
  }

  onEditBottle(index: number) {
    this.blService.startedEditing.next(index);
  }



}
