import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BottleItemComponent } from './bottle-item.component';

describe('BottleItemComponent', () => {
  let component: BottleItemComponent;
  let fixture: ComponentFixture<BottleItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BottleItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BottleItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
