import { Component, Input, OnInit } from '@angular/core';
import { Bottle } from '../../../bottle/bottle.model';

@Component({
  selector: 'app-bottle-item',
  templateUrl: './bottle-item.component.html',
  styleUrls: ['./bottle-item.component.css']
})
export class BottleItemComponent implements OnInit {
  @Input() bottle: Bottle;
  @Input() index: number;

  ngOnInit() {
  }

}
