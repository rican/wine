import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BottlesComponent } from './bottles/bottles.component';
import { BottleDetailComponent } from './bottles/bottle-detail/bottle-detail.component';
import { BottleEditComponent } from './bottles/bottle-edit/bottle-edit.component';


const appRoutes: Routes = [
  { path: '', redirectTo: '/bottles', pathMatch: 'full' },
  { path: 'bottles', component: BottlesComponent, children: [
    { path: 'new', component: BottleEditComponent },
    { path: ':id', component: BottleEditComponent },
    { path: ':id/edit', component: BottleEditComponent }

  ] },
];


@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
