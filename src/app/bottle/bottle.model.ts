export class Bottle {
  public name: string;
  public year: number;
  public country: string;
  public region: string;
  public aoc: string;
  public number: number;


  constructor(name: string, year: number, country: string, region: string, aoc: string, num: number) {
    this.name = name;
    this.year = year;
    this.country = country;
    this.region = region;
    this.aoc = aoc;
    this.number = num;

  }

}
