import { WinoPage } from './app.po';

describe('wino App', () => {
  let page: WinoPage;

  beforeEach(() => {
    page = new WinoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
